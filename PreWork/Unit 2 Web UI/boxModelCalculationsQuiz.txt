#div1 {

height: 150px;

width: 400px;

margin: 20px;

border: 1px solid red;

padding: 10px;

}

Total Height:
Formula: margin-top + border-top + padding-top + (height of the content)+ 
padding-bottom + border-bottom + margin-bottom

20 + 1 + 10 + 150 + 10 + 1 + 20 = 212 (Total Height) 

Total Width: 
Formula: margin-left + border-left + padding-left + (width of the 
content)+  padding-right + border-right + margin-right

20 + 1 + 10 + 400 + 10 + 1 + 20 = 462 (Total Width) 

Browser Calculated Height: 
Formula: border-top + padding-top + (height of the 
content)+ padding-bottom + border-bottom

1 + 10 + 150 + 10 + 1 = 172 (Browser Calculated Height) 

Browser Calculated Width:
Formula: border-left + padding-left + (width of the 
content)+  padding-right + border-right

1 + 10 + 400 + 10 + 1 = 422 (Browser Calculated Width) 
